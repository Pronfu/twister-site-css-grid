Thanks to the following for their guides / help. Even if it just was a simple blog post.

[Jen Simmons Space Jam on CSS Grid](https://labs.jensimmons.com/2017/02-015.html) as that inspired this project.
[Codepen link](https://codepen.io/jensimmons/pen/yggmyY)

[CSS Grid: Holy Grail Layout - Responsive by Geoff Graham](https://codepen.io/geoffgraham/pen/yPYKzw/) for the template for the project.

[CSS-Tricks alanac on letter-spacing](https://css-tricks.com/almanac/properties/l/letter-spacing/)

[color-hex](https://www.color-hex.com) for their awesome website

[CSS-Tricks Snippets on Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) I would be lost without it.

[CSS Link Color](https://htmlcolorcodes.com/tutorials/css-link-color/) on htmlcolorcodes.com for the trick to get a link to be a certain colour when needed.

[LayoutIt CSS Grid Generator](https://www.layoutit.com/grid) for grid generation for certain pages. 

[font-weight on MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight)

[html5-tutorial ul and ol tags article](https://www.html-5-tutorial.com/ul-ol-tags.htm)

Every other site that I linked to through the pages, as I didn't want to recreate things that already existed.