# Twister Site Using CSS Grid

This is a recreation of the [original TWISTER movie website](https://web.archive.org/web/20010124070900/http://movies.warnerbros.com/twister/), done to teach you [CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/) and [21st century design](https://stuffandnonsense.co.uk/blog/designing-your-website-like-its-2018) . All the credit to the original TWISTER website and [Warner Bros. Movies](https://www.warnerbros.com/).

## Installation

This is a static website, you can just display these files in your browser. Edit using any program you want.

## Live Demo
[https://projects.gregoryhammond.ca/twister-site-css-grid/](https://projects.gregoryhammond.ca/twister-site-css-grid/)

## License
[Unlicense](http://unlicense.org/) and/or [WTFPL](http://www.wtfpl.net/). Use whichever license you see fit, they both do basically the same thing.